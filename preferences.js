define([
	'require',
	'madjoh_modules/controls/controls',
	'madjoh_modules/custom_events/custom_events',
	'madjoh_modules/ajax/ajax',
	'madjoh_modules/msgbox/msgbox',
	'madjoh_modules/language/language',
	'settings/game/game',
	'settings/preferences/preferences',
	'madjoh_modules/wording/wording',
],
function(require, Controls, CustomEvents, AJAX, CustomAlert, Language, GameSettings, PreferencesSettings, Wording){
	Preferences = {
		init : function(){
			document.addEventListener('online', Preferences.uploadReported, false);
		
			element = document.querySelectorAll('.playOfflineButton');
			for(var i=0; i<element.length; i++) element[i].addEventListener(Controls.released, Preferences.offline, false);
		},
		
		store : function(appSave){
			document.preferences = appSave;
			localStorage.setItem(GameSettings.gameBundle+'.preferences', JSON.stringify(appSave)); 
		},

		sync : function(callback){
			AJAX.post(
				'/user/profile/sync',
				{callback : callback, auth : true}
			);
		},

		save : function(){
			localStorage.setItem(GameSettings.gameBundle+'.preferences', JSON.stringify(document.preferences));
			if(document.preferences.user.mail) Preferences.upload();
		},

		upload : function(){
			if(GameSettings.isArcade){
				var parameters = {
					more : JSON.stringify(document.preferences.arcade_save)
				};

				AJAX.post(
					'/appsave/arcade',
					{
						callback 	: Preferences.uploadCallback,
						parameters 	: parameters,
						auth 		: true
					}
				);
			}
		},
		uploadCallback : function(result){
			if(result.status===1){
				localStorage.uploadPreferencesError = false;
			}else{
				if(result) console.log(result.errors);
				localStorage.uploadPreferencesError = true;
				CustomAlert({text : Wording.messages.upload_failed[Language.get()], onOk : Preferences.upload});
			}
		},
		uploadReported : function(){
			if(localStorage.uploadPreferencesError){
				CustomAlert({text : Wording.messages.upload_recovering[Language.get()]});
				Preferences.upload();
			}
		},

		offline : function(){
			Preferences.store(PreferencesSettings.getBasicPref());
			CustomEvents.fireCustomEvent(document, 'LoggedIn');
		},

		forget : function(){
			Preferences.upload();
			localStorage.removeItem(GameSettings.gameBundle+'.preferences');
			document.preferences = null;
		}
	};
	return Preferences;
});